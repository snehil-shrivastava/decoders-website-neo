module.exports = {
  siteMetadata: {
    title: `DeCoders`,
    description: `Siddaganga Institute of Technology Official Programming club`,
    author: `DeCoders Group`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-styled-components`,
    `gatsby-plugin-polished`,
    {
      resolve: `@el7cosmos/gatsby-plugin-prefetch-google-fonts`,
      options: {
        fonts: [
          {
            family: `Montserrat`,
            variants: [`400`, `700`],
          },
          {
            family: `IBM+Plex+Sans`,
            variants: [`400`, `700`],
          },
          {
            family: `IBM+Plex+Mono`,
            variants: [`400`],
          },
        ],
      },
    },
    `gatsby-plugin-layout`,
    {
      resolve: `gatsby-plugin-mdx`,
      options: {
        extensions: [`.mdx`, `.md`],
        plugins: [`gatsby-remark-auto-headers-improved`],
        gatsbyRemarkPlugins: [`gatsby-remark-auto-headers-improved`],
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `site`,
        path: `${__dirname}/src/content/site`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-react-svg`,
    `gatsby-plugin-remove-trailing-slashes`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `The DeCoders`,
        short_name: `DeCoders`,
        start_url: `/`,
        background_color: `#1f2a34`,
        theme_color: `#2874fe`,
        display: `minimal-ui`,
        icon: `src/images/icon.png`,
      },
    },
  ],
}
