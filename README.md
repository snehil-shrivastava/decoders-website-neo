# Decoders Website Neo

Website for DeCoders using Gatsby  

Website is forked from [TPH](https://github.com/the-programmers-hangout/website/issues)


### setup

1. Clone the repo
1. npm install
1. npm run develop
1. Nagivate to http://localhost:8000

### Adding a new resource

Gatsby will take care of most of this process by creating a new page and positioning it in the sidebar automatically once you write up a new resource. All you have to do is follow these steps:

    Create a markdown page in the appropriate location. For example /src/content/docs/haskell/monads.md
    Add the required frontmatter for the post. More info here
    Write your main content.
    Add external_resources about the subject if possible.
    Run through the setup steps if possible to make sure your changes look ok.
    Create a pull request.

### Frontmatter

Frontmatter is the optional metadata attached to every markdown file, like the list of authors or the creation date of the file. It is essentially just yaml syntax inside --- delimiters within a markdown file. If you're not familiar with yaml you can find some refreshers here

An example frontmatter might look like this

```md
---
authors:
  - "Xetera#0001"
title: "Working with Lists in Elixir"
created_at: 2020/01/30
external_resources:
  - text: Elixir docs: List
    href: "https://hexdocs.pm/elixir/List.html"
---
# Title
some content here

You can look at other resources like this one for reference when creating your own. We use ISO8601 as the date format to confuse both Americans and Europeans an equal amount.
```

*All the information above has been copied from the TBH project*

for backlogs and issue, go to project's jira page.
