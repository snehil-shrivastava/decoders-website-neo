import { ApolloClient } from "@apollo/client";
import { IconDefinition } from "@fortawesome/free-brands-svg-icons";
import { ChangeEventHandler, FocusEventHandler, InputHTMLAttributes } from "react";
import { CardLinks, CardPlatform } from "../components/Card/types";

export interface IFile {
  title: string
  type: "file"
  path: string
}

export interface IFolder {
  title: string
  type: "folder"
  path: string
  children: IFileOrFolder[]
}

export type IFileOrFolder = IFile | IFolder

export interface IFileQuery {
  node: {
    relativePath: string
  }
}

export interface IFileResourceQuery {
  node: {
    relativePath: string
    relativeDirectory: string
    childMdx: {
      frontmatter: {
        author: string
        date: string
      }
    }
  }
}

export interface IFileArchiveQuery {
  node: {
    relativePath: string
  }
}

export interface IAllFilesQuery {
  allFile: {
    edges: IFileQuery[]
  }
}

export interface IAllResourcesQuery extends IAllFilesQuery {
  allFile: {
    edges: IFileResourceQuery[]
  }
}

export interface IAllArchivesQuery extends IAllFilesQuery {
  allFile: {
    edges: IFileArchiveQuery[]
  }
}

export interface IExternalResource {
  text: string
  href: string
}

export interface ITocItem {
  depth: number
  link: string
  title: string
}

export interface ICardLinksProp {
  link: string
  icon: IconDefinition
}

export interface IProfileTypes {
  image?: string,
  text?: string,
  name?: string
}


export interface IProfilePage {

}

export interface IGqlMembersList {
    members: Array<IGqlMember>
    year: Number
}

export interface IGqlMember{
  name: string
  id: string
  photourl: string
  socialLinks: [CardLinks]
  aboutme_short: string
}


export interface ITeamsFragmentProps{
  list: [IGqlMembersList]
}