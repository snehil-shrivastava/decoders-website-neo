import { graphql } from "gatsby"
import React, { Fragment, FC } from "react"
import cx from "classnames"
import { Mdx } from "../../generated/graphql"
import { ComponentQuery } from "../../typings"
import { HeaderBarebone } from "../components/HeaderBarebone"
import { Markdown } from "../components/Markdown"
import { PageContent } from "../components/PageContent"
import { SEO } from "../components/SEO"
import { buildToc, buildTocForTeams } from "../utils"
import { Box, Grid } from "react-raster"
import { Card } from "../components/Card"
import { GET_MEMBERS_BY_YEAR, GET_PROFILE_SIDEBAR_CONTENT } from "../api/DeCodersApi/graphql";
import { IGqlMember, IGqlMembersList, ITeamsFragmentProps } from "../types"
import { ApolloConsumer, useQuery } from "@apollo/client"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faLink } from "@fortawesome/free-solid-svg-icons"
import { Anchor } from "../components/PageSidebarLink/styles"

const TeamFragment: FC<ITeamsFragmentProps> = ({ children, list }) => {
  return <>{list.map((x: IGqlMembersList) => {
    return (<div key={x.year.toString()}>
      <h2>
        <Anchor href={`#${x.year}`} aria-hidden="true" className="anchor">
        <small><FontAwesomeIcon icon={faLink}></FontAwesomeIcon></small>
        </Anchor>
      {x.year}</h2>
      <Grid
        colspan={[6, 6, 12]}
        left={"2vw"}
        right={"2vw"}
        top={"2vw"}
        bottom={"2vw"}
        gutterX={"2vw"}
        gutterY={"2vw"}
        control
      >
        {
          x.members.map((y: IGqlMember) => {
            return <Box
              cols={[3, 3, 6]}
              alignX="center"
              alignY="center"
              key={y.id}
            >
              <Card name={y.name} image={y.photourl} social={y.socialLinks}>{y.aboutme_short}</Card>
            </Box>
          })
        }
      </Grid>
    </div>)
  })
  }</>
}

function Content() {
  const { data, loading, error } = useQuery(GET_MEMBERS_BY_YEAR)
  if (error) return <div>{JSON.stringify(error)}</div>
  if (loading) return <></>
  const list = data.listMember as [IGqlMembersList]
  return (<PageContent content={<TeamFragment list={list} />} toc={buildTocForTeams(list)} />)
}

function TeamPage() {

  return (
    <Fragment>
      <SEO title="Team" />
      <HeaderBarebone title="Team" className={cx({ shifted: 10 })} />
      <ApolloConsumer>
        {
          client => <Content/>
        }
      </ApolloConsumer>
    </Fragment>
  )
}

export const query = graphql`
  query TeamPage {
    md: mdx(frontmatter: { path: { eq: "/team" } }) {
      body
      headings {
        depth
        value
      }
    }
  }
`

export default TeamPage
