import React, { Fragment, Component } from "react"
import cx from "classnames"
import { SEO } from "../components/SEO"
import { HeaderBarebone } from "../components/HeaderBarebone"
import { PageContent } from "../components/PageContent"
import styled from "styled-components"

function FeedPage(){
    return (
        <Fragment>
            <SEO title="Feeds" />
            <HeaderBarebone title="Feeds" className={cx({ shifted: 0 })} />
            <PageContent content={
                <div>

                </div>
            } />
        </Fragment>
    )
}

export default FeedPage