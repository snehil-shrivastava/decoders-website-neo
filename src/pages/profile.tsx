import { graphql } from "gatsby"
import React, { Fragment, FC } from "react"
import cx from "classnames"
import { Mdx } from "../../generated/graphql"
import { ComponentQuery } from "../../typings"
import { Markdown } from "../components/Markdown"
import { SEO } from "../components/SEO"
import { PageContent } from "../components/PageContent"
import { HeaderBarebone } from "../components/HeaderBarebone"
import { buildToc } from "../utils"
import { IProfileTypes } from "../types"
import { RequiresLogin } from "../components/Auth"
import { ApolloConsumer } from "@apollo/client"


const ProfilePageInner: FC<IProfileTypes> = ({ children }) => {
  return (<>

  </>)
}

function ProfilePage() {
  return <ApolloConsumer>
    {
      client => <RequiresLogin>
        <ProfilePage />
      </RequiresLogin>
    }
  </ApolloConsumer>
}

export default ProfilePage
