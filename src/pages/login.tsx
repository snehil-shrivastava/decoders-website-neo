import React, { Fragment, Component, FC } from "react"
import cx from "classnames"
import { SEO } from "../components/SEO"
import { HeaderBarebone } from "../components/HeaderBarebone"
import { PageContent } from "../components/PageContent"
import { ApolloClient, ApolloConsumer, useQuery } from "@apollo/client"
import { login } from "../api/DeCodersApi"
import { Formik } from "formik"
import { navigate } from "gatsby"
import { signinSchema } from '../validator/globalValidator'
import { Link } from "gatsby"
import { GET_ID } from "../api/DeCodersApi/graphql"
import { Grid, Form, Field, Icon, Hidden, Input, Icons } from "../components/FormComponents"
import { faLock, faUserAlt } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { RequiresLogin, RequiresNotLogin } from "../components/Auth"
import IconicInput from "../components/IconicInput"

class LoginPageInner extends Component {

    render() {
        return (
            <>
                <Grid>
                    <Formik
                        initialValues={{ username: '', password: '' }}
                        validationSchema={signinSchema}
                        onSubmit={async (values, { setSubmitting }) => {
                            setSubmitting(true);
                            try {
                                login(values.username, values.password)
                                    .then(() => {
                                        navigate('/feed')
                                        setSubmitting(false);
                                    })
                                    .catch((e) => {
                                        navigate('/feed')
                                        setSubmitting(false);
                                    })
                            } catch (e) {
                                navigate('/feed')
                            }
                        }}>
                        {({
                            values,
                            errors,
                            touched,
                            handleChange,
                            handleBlur,
                            handleSubmit,
                            isSubmitting,
                        }) => (
                            <Form onSubmit={handleSubmit} onChange={handleChange} onBlur={handleBlur}>
                                <IconicInput
                                    name="username"
                                    onBlur={handleBlur}
                                    onChange={handleChange}
                                    icon={faUserAlt}
                                    placeholer={"Username"}
                                    value={values.username}
                                    touched={touched.username}
                                    error={errors.username}
                                    type="text" />
                                <IconicInput
                                    name="password"
                                    onBlur={handleBlur}
                                    onChange={handleChange}
                                    icon={faLock}
                                    placeholer={"Password"}
                                    value={values.password}
                                    touched={touched.password}
                                    error={errors.password}
                                    type="password" />
                                <Field>
                                    <button type="submit" disabled={isSubmitting}>
                                        Sign In
                                    </button>
                                </Field>
                                <Field>
                                    <span>No Account ? <Link to="/signup">Signup</Link></span>
                                </Field>
                            </Form>
                        )}
                    </Formik>
                </Grid>

            </>
        )
    }
}

function LoginPage() {
    return (<ApolloConsumer>{client =>
        <RequiresNotLogin >
            <Fragment>
                <SEO title="Login" />
                <HeaderBarebone title="Login" className={cx({ shifted: 0 })} />
                <PageContent content={
                    <LoginPageInner />} />
            </Fragment>
        </RequiresNotLogin>}</ApolloConsumer>)
}

export default LoginPage
