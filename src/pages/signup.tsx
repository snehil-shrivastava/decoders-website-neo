import React, { Fragment, Component, FC, useState } from "react"
import { ApolloClient, ApolloConsumer, useQuery } from "@apollo/client"
import { Formik } from "formik"
import { CardPlatform } from "../components/Card/types"
import { Link, redirectTo } from "@reach/router"
import { signupSchema } from "../validator/globalValidator"
import { Field, FieldUpload, Form, Hidden, Icon, Input } from "../components/FormComponents"
import { faIdBadge, faLock, faUpload, faUserAlt } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { RequiresNotLogin } from "../components/Auth"
import IconicInput from "../components/IconicInput"
import UploadComponent from "../components/UploadComponent"
import { any } from "ramda"
import { SEO } from "../components/SEO"
import { HeaderBarebone } from "../components/HeaderBarebone"
import cx from "classnames"
import { PageContent } from "../components/PageContent"
import { faFantasyFlightGames } from "@fortawesome/free-brands-svg-icons"
import * as fs from "fs"

/*

<Formik initialValues={{ username: '', password: '' }}
    validationSchema={signinSchema}
    onSubmit={async (values, { setSubmitting }) => {
        setSubmitting(true);
        try {
            login(values.username, values.password)
                .then(() => {
                    navigate('/feed')
                    setSubmitting(false);
                })
                .catch((e) => {
                    navigate('/feed')
                    setSubmitting(false);
                })
        } catch (e) {
            redirectTo('/feed')
        }
    }}>
    {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
    }) => ( <Form>

    </Form> )
    </Formik>

*/
function SignUpPageHolder() {
    return <Formik
        initialValues={{ username: '', password: '', passwordConfirmation: '', name: '', usn: '', file: undefined }}
        validationSchema={signupSchema}
        onSubmit={async (values, { setSubmitting }) => {
            setSubmitting(true);
            try {

            } catch (e) {
                redirectTo('/login')
            }
        }}>
        {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            setFieldValue,
        }) => {
            return (
                <Form onSubmit={handleSubmit} onChange={handleChange} onBlur={handleBlur}>
                    <IconicInput
                        name="username"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        icon={faUserAlt}
                        placeholer={"Username"}
                        value={values.username}
                        touched={touched.username}
                        error={errors.username}
                        type="text" />
                    <IconicInput
                        name="password"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        icon={faLock}
                        placeholer={"Password"}
                        value={values.password}
                        touched={touched.password}
                        error={errors.password}
                        type="password" />
                    <IconicInput
                        name="passwordConfirmation"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        icon={faLock}
                        placeholer={"Confirm Password"}
                        value={values.passwordConfirmation}
                        touched={touched.passwordConfirmation}
                        error={errors.passwordConfirmation}
                        type="password" />
                    <IconicInput
                        name="name"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        icon={faFantasyFlightGames}
                        placeholer={"Name"}
                        value={values.name}
                        touched={touched.name}
                        error={errors.name}
                        type="text" />
                    <IconicInput
                        name="usn"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        icon={faIdBadge}
                        placeholer={"Usn"}
                        value={values.usn}
                        touched={touched.usn}
                        error={errors.usn}
                        type="text" />
                        <FieldUpload>
                            <label>
                                <FontAwesomeIcon icon={faUpload}/>
                                <span>Profile Pic</span>
                            </label>
                            <UploadComponent 
                                setFieldValue={setFieldValue} 
                                activeTxt="Drop file here"
                                inactiveTxt={`Click or drag to ${values.file ? "replace uploaded" : "upload"}  file`}/>
                            {values.file && 
                            <div> 
                                <p>{`File:${(values.file as unknown as File).name}` }{ }</p>
                                <img src={URL.createObjectURL(values.file as unknown as File)}/>
                            </div>}
                        </FieldUpload>
                    <Field>
                        <button type="submit" disabled={isSubmitting}>
                            Sign Up
                    </button>
                    </Field>
                </Form>)
        }}
    </Formik>
}

function SignUpPage() {

    return (
        <ApolloConsumer>{client => <RequiresNotLogin><Fragment>
            <SEO title="Signup" />
            <HeaderBarebone title="Signup" className={cx({ shifted: 0 })} />
            <PageContent content={  <SignUpPageHolder />} />
        </Fragment></RequiresNotLogin>}</ApolloConsumer>
    )
}

export default SignUpPage
