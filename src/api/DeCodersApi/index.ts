import { ApolloClient, HttpLink, InMemoryCache} from '@apollo/client';
import { SocialLinks, SocialMediaPlatform } from "../../components/Card";
import { setContext } from '@apollo/client/link/context';
import fetch from 'cross-fetch';
import * as gql from './graphql'

export async function login(username: String, password: String) {
  var urlencoded = new URLSearchParams();
  urlencoded.append("username", "snehil");
  urlencoded.append("password", "worthit");

  return fetch(`${process.env.GATSBY_SERVER_URI}login`, {
    credentials: 'include',
    method: "POST",
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Upgrade-Insecure-Requests": "1",
      "Content-Type": "application/x-www-form-urlencoded",
      "Accept-Language": "en-US,en;q=0.5",
      "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
      "Origin": "http://localhost:1080"
    },
    body: urlencoded,
    redirect: 'follow',
  })
}

const client = new ApolloClient({
  link: new HttpLink({
    uri: `${process.env.GATSBY_SERVER_URI}graphql`,
    fetch,
    includeUnusedVariables: true,
    credentials: 'include'
  }),
  credentials: 'include',
  cache: new InMemoryCache(),
  connectToDevTools: true
});

export default client;
