/* 

*/

import gql from "graphql-tag";

export const GET_PROFILE_SIDEBAR_CONTENT = gql`
  query{
    me {
        name
        designation
        photourl
    }
  }
`;

export const POST_LOGOUT = gql`
  mutation{
    logout
  }
`;

export const GET_ID = gql`
query{
    me {
      id
    }
}
`

export const GET_MEMBERS_BY_YEAR = gql`
query {
  listMember{
      members {
        name
        id
      socialLinks{
        link
        platform
      }
      photourl
        aboutme_short
      }
      year
    }
  }
`