---
path: /about
---

import { DiscordButton } from "../../components/DiscordButton"

## What is Club DeCoders?

DeCoders is the official programming club of Siddaganga Institute of Technology, Tumkur, under the Department of Computer Science and Engineering. DeCoders community geared towards programming. The use of the word "geared" here is important because more accurately it's a discord for programmers of all kinds. If you're a green noob with 5 lines of code under your belt, or if you're a veteran with 15 years of industry experience, DeCoders has a place for you.

## How do I join?

// TODO

<DiscordButton>TODO JOIN</DiscordButton>

## When was this server made?

This server was made on the 19th of April, 2021.

## Am I welcome?

Anyone interested in discussing programming and not breaking the [rules](/rules) is welcome.
