import { IconDefinition } from "@fortawesome/fontawesome-common-types"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { FormikValues } from "formik"
import React, { ChangeEventHandler, FC, FocusEventHandler } from "react"
import { Field, Hidden, Icon, Input } from "../FormComponents"

const IconicInput: FC<{
    name: string,
    placeholer: string,
    icon: IconDefinition,
    onChange: ChangeEventHandler<any>,
    onBlur: FocusEventHandler<any>,
    value: string,
    error: string | undefined,
    touched: boolean | undefined,
    type: string
}> = ({ name, icon, placeholer, onChange, onBlur, value, touched, error, type}) => {
    return <>
        <Field>
            <label htmlFor={name}>
                <Icon>
                    <FontAwesomeIcon icon={icon} />
                </Icon>
                <Hidden>Username</Hidden>
            </label>
            <Input
                id={name} type={type} name={name} placeholder={placeholer}
                onChange={onChange}
                onBlur={onBlur}
                value={value}
            />
        </Field>
        <small>
            {error && touched && error}
        </small>
    </>
}

export default  IconicInput;