import React, { FC, HTMLAttributes } from "react"
import Scrollbar from "react-perfect-scrollbar"
import "react-perfect-scrollbar/dist/css/styles.css"
import cx from "classnames"
import { ThemeToggler } from "../ThemeToggler"
import * as SC from "./styles"
import { LoginSideBar } from "../LoginSideBar"
import { ApolloConsumer, useQuery } from '@apollo/client';

interface IMenuItemProps {
  to: string
}

const MenuItem: FC<IMenuItemProps> = ({ children, to }) => {
  return (
    <SC.MenuItem to={to} activeClassName="active">
      {children}
    </SC.MenuItem>
  )
}

export const Sidebar: FC<HTMLAttributes<HTMLDivElement>> = (props) => {
  const { children, ...restProps } = props

  return (
    <SC.SidebarWrapper {...restProps}>
      <Scrollbar>
        <SC.Header to="/">
          <SC.Logo>
            <SC.StyledLogoSmall />
          </SC.Logo>
          <SC.Title>
            DeCoder&apos;s
          </SC.Title>

        </SC.Header>

        <SC.Inner>

          <ApolloConsumer>
            {client => <LoginSideBar />}
          </ApolloConsumer>
          {children}

          <SC.Menu className={cx({ "has-border": Boolean(children) })}>
            <MenuItem to="/about">about</MenuItem>
            <MenuItem to="/team">team</MenuItem>
            <MenuItem to="/rules">rules</MenuItem>
            <MenuItem to="/beginners">beginners</MenuItem>
            <MenuItem to="/faq">faq</MenuItem>
          </SC.Menu>

          <ThemeToggler />
        </SC.Inner>
      </Scrollbar>
    </SC.SidebarWrapper>
  )
}
