import styled from "styled-components";
import { transparentize } from "polished";

export const Grid = styled.div`
width: 90%;
max-width: 20rem;
`;
export const Form = styled.form`
    --htmlFontSize: 100%;
    --iconFill: ${props => props.theme.sidebar.foreground};
    --bodyBackgroundColor: #2c3338;
    --bodyColor: var(#606468);
    --bodyFontFamily: "Open Sans";
    --bodyFontFamilyFallback: sans-serif;
    --bodyFontSize: 0.875rem;
    --bodyFontWeight: 400;
    --bodyLineHeight: 1.5;
    --loginBorderRadus: 0.25rem;
    --loginColor: ${props => props.theme.sidebar.foreground};
  
    --loginInputBackgroundColor: ${props => transparentize(0.8, props.theme.sidebar.foreground)};
    --loginInputHoverBackgroundColor: ${props => transparentize(0.9, props.theme.sidebar.foreground)};
  
    --loginLabelBackgroundColor: ${props => transparentize(0.9, props.theme.sidebar.foreground)};
  
    --loginSubmitBackgroundColor: ${props => props.theme.discord.base};
    --loginSubmitColor: ${props => '#fff'};
    --loginSubmitHoverBackgroundColor: ${props => props.theme.discord.darker};
    small {
        margin-left: 0.875rem;
        color: red;
    }

input {
    background-image: none;
    border: 0;
    color: inherit;
    font: inherit;
    margin: 0;
    outline: 0;
    padding: 0;
    -webkit-transition: background-color 0.3s;
    -o-transition: background-color 0.3s;
    transition: background-color 0.3s;
  }
  
  button {
    cursor: pointer;
  }  
  input[type="password"],
  input[type="text"],
  button {
    width: 100%;
  }

  input[type="password"],
  input[type="text"] {
    background-color: #3b4148;
    background-color: var(--loginInputBackgroundColor);
    border-bottom-left-radius: 0;
    border-top-left-radius: 0;
}

  label,
  input[type="text"],
  input[type="password"],
  button {
    border-radius: 0.25rem;
    border-radius: var(--loginBorderRadus);
    padding: 1rem;
  }

  label,
  input[type="text"],
  input[type="password"],
  button {
    border-radius: 0.25rem;
    border-radius: var(--loginBorderRadus);
    padding: 1rem;
  }

  button {
    background-color: #ea4c88;
    background-color: var(--loginSubmitBackgroundColor);
    color: #eee;
    color: var(--loginSubmitColor);
    font-weight: 700;
    text-transform: uppercase;
    border: none;

    &:focus,
    &:hover {
        background-color: #d44179;
        background-color: var(--loginSubmitHoverBackgroundColor);
    }
  }
  
  
  p {
    margin-top: 1.5rem;
    margin-bottom: 1.5rem;
  }
  
  label {
    background-color: #363b41;
    background-color: var(--loginLabelBackgroundColor);
    border-bottom-right-radius: 0;
    border-top-right-radius: 0;
    padding-left: 1.25rem;
    padding-right: 1.25rem;
  }

  input[type="password"],
input[type="text"] {
    background-color: #3b4148;
    background-color: var(--loginInputBackgroundColor);
    border-bottom-left-radius: 0;
    border-top-left-radius: 0;
}
`;
export const Field = styled.div`
display: -webkit-box;
display: -ms-flexbox;
display: flex;
margin: 0.875rem;
margin: 0.875rem;
`;
export const Input = styled.input`
-webkit-box-flex: 1;
-ms-flex: 1;
    flex: 1;
`;
export const Hidden = styled.span`
border: 0;
clip: rect(0 0 0 0);
height: 1px;
margin: -1px;
overflow: hidden;
padding: 0;
position: absolute;
width: 1px;
`;
const TextCenter = styled.p`
text-align: center;
`;
export const Icon = styled.svg`
height: 1em;
display: inline-block;
fill: #606468;
fill: var(--iconFill);
width: 1em;
vertical-align: middle;
`;
export const Icons = styled.svg`
display: none;
`;

export const FieldUpload = styled(Field)`
  flex-direction: column;
  img {
    width: 100px; 
  }
  label {
    border-radius: var(--loginBorderRadus);
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
    span {
      padding-left: 2rem;
    }
  }
  div {
    border-radius: var(--loginBorderRadus);
    background-color: var(--loginInputBackgroundColor);
    padding: 1rem;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
  }
`