import { ApolloConsumer, useQuery } from "@apollo/client";
import { navigate } from "gatsby";
import { FC } from "react";
import client from "../../api/DeCodersApi";
import { GET_ID } from "../../api/DeCodersApi/graphql";

import React from 'react'



export const RequiresLogin: FC<{fallbackUrl?: string}> = ({ children, fallbackUrl = "/404"}) => {
    const { data, loading } = useQuery(GET_ID)
    if (loading)
      return (<div></div>);
  
    if (data == undefined) navigate(fallbackUrl)

    return <ApolloConsumer>{client=> <>{children}</>}</ApolloConsumer>
}

export const RequiresNotLogin: FC<{fallbackUrl?: string}> = ({ children, fallbackUrl = "/404"}) => {
  const { data, loading } = useQuery(GET_ID)
  if (loading)
    return (<div></div>);
  if (data != undefined) navigate(fallbackUrl)

  return <ApolloConsumer>{client=> <>{children}</>}</ApolloConsumer>
}