import React, { FC } from "react"
import styled from "styled-components"
import { ICardLinksProp } from "../../types"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { IconDefinition } from "@fortawesome/free-brands-svg-icons"

export const Wrapper = styled.div`
@import url(https://fonts.googleapis.com/css?family=Quicksand:400,500,700&subset=latin-ext);
    padding: 50px 20px 0px 0px;
    padding-top: 100px;
    display: flex;
    font-family: Quicksand, sans-serif;
    color: #324e63
    @media screen and (max-width:768px) {
    		height: auto;
    		padding-top: 100px
    }
`

export const ProfileCard = styled.div`
min-width: 300px;
margin: auto;
box-shadow: 0 8px 60px -10px rgba(13, 28, 39, .6);
background: #fff;
border-radius: 12px;
max-width: 700px;
position: relative
`


export const ProfileCardImage = styled.div`
width: 150px;
    height: 150px;
    margin-left: auto;
    margin-right: auto;
    transform: translateY(-50%);
    border-radius: 50%;
    overflow: hidden;
    position: relative;
    z-index: 4;
    box-shadow: 0px 5px 50px 0px rgb(108, 68, 252), 0px 0px 0px 7px rgba(107, 74, 255, 0.5);

    @media screen and (max-width: 576px) {
      width: 120px;
      height: 120px;
    }
img{
    display: block;
	width: 100%;
	height: 100%;
	object-fit: cover;
	border-radius: 50%
}
`
export const ProfileCardContainer = styled.div`
margin-top: -35px;
text-align: center;
padding: 0 20px;
padding-bottom: 40px;
transition: all .3s
`

export const ProfileCardName = styled.div`

font-weight: 700;
font-size: 24px;
color: #6944ff;
margin-bottom: 15px
`

export const ProfileCardText = styled.div`

font-size: 18px;
font-weight: 500;
color: #324e63;
margin-bottom: 15px;
strong {
	font-weight: 700
}

`

export const ProfileCardSocial = styled.div`

margin-top: 25px;
display: flex;
justify-content: center;
align-items: center;
flex-wrap: wrap

`
export const ProfileCardSocialItem = styled.a`
display: inline-flex;
width: 55px;
height: 55px;
margin: 15px;
border-radius: 50%;
align-items: center;
justify-content: center;
color: #fff;
position: relative;
font-size: 21px;
flex-shrink: 0;
padding: 2px;
transition: all .3s;
@media screen and (max-width:768px) {
		width: 50px;
		height: 50px;
		margin: 10px
    }

    @media screen and (min-width: 768px){
        &:hover {
          transform: scale(1.2);
        }
      }
`

const social = (props: ICardLinksProp) => (
<ProfileCardSocialItem href={props.link}>
    <span style={{ display: "inline-flex" }}><FontAwesomeIcon icon={props.icon} size="1x"/></span>
</ProfileCardSocialItem>)

export const Facebook = styled(social) <{ link: string, icon: IconDefinition}>`
background: linear-gradient(45deg, #3b5998, #0078d7);
box-shadow: 0 4px 30px rgba(43, 98, 169, .5)
`

export const Twitter = styled(social) <{ link: string, icon: IconDefinition}>`
background: linear-gradient(45deg, #1da1f2, #0e71c8);
box-shadow: 0 4px 30px rgba(19, 127, 212, .7)
`


export const Insta = styled(social) <{ link: string, icon: IconDefinition}>`
background: linear-gradient(45deg, #405de6, #5851db, #833ab4, #c13584, #e1306c, #fd1d1d);
box-shadow: 0 4px 30px rgba(120, 64, 190, .6)
`

export const Github = styled(social) <{ link: string, icon: IconDefinition}>`
background: linear-gradient(45deg, #333, #626b73);
box-shadow: 0 4px 30px rgba(63, 65, 67, .6)
`

export const EMail = styled(social) <{ link: string, icon: IconDefinition}>`
background: linear-gradient(45deg, #d5135a, #f05924)!important;
box-shadow: 0 4px 30px rgba(223, 45, 70, .6)!important
`



