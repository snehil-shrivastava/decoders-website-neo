import React, { FC } from "react"
import * as SC from "./styles"
import { faFacebookF, faInstagram, faGithubAlt } from '@fortawesome/free-brands-svg-icons'
import { faEnvelopeOpen } from '@fortawesome/free-solid-svg-icons'
import { CardPlatform as Platform, CardLinks } from "./types"

export const Card: FC<{ name: string, image: string, social: [CardLinks] }> = ({ children, name, image, social }) => {
    return <SC.Wrapper>
        <SC.ProfileCard>
            <SC.ProfileCardImage>
                <img src={image} alt="profile card" />
            </SC.ProfileCardImage>
            <SC.ProfileCardContainer>
                <SC.ProfileCardName>{name}</SC.ProfileCardName>
                <SC.ProfileCardText>{children}</SC.ProfileCardText>
                <SC.ProfileCardSocial>
                    {
                        social.map(({link, platform}) => {
                            switch (platform) {
                                case Platform.facebook:
                                    return <SC.Facebook key={'facebook'} link={link} icon={faFacebookF} />
                                case Platform.github:
                                    return <SC.Github key={'github'} link={link} icon={faGithubAlt} />
                                case Platform.instagram:
                                    return <SC.Insta key={'insta'} link={link} icon={faInstagram} />
                                case Platform.email:
                                    return <SC.EMail key={'email'} link={link} icon={faEnvelopeOpen} />
                                default:
                                    break;
                            }
                        })
                    }
                </SC.ProfileCardSocial>
            </SC.ProfileCardContainer>
        </SC.ProfileCard>
    </SC.Wrapper>
}
