import { faFacebookF, IconDefinition } from "@fortawesome/free-brands-svg-icons";

export enum CardPlatform {
  facebook,
  linkedin,
  email, 
  twitter, 
  instagram, 
  reddit,
  github
}



interface PlatfromProps {
  color: String,
  icon: IconDefinition
}
  
  
export interface CardLinks{
  link: string
  platform: CardPlatform
}