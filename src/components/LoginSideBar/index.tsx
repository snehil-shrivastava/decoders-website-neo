import React, { FC, useState } from "react"
import * as SC from "./styles"
import { ApolloConsumer, useQuery } from '@apollo/client';
import client from "../../api/DeCodersApi";
import { GET_PROFILE_SIDEBAR_CONTENT, POST_LOGOUT } from "../../api/DeCodersApi/graphql";
import { IProfileTypes } from "../../types";
import {Link, navigate} from "gatsby";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUserLock } from "@fortawesome/free-solid-svg-icons";

const Profile: FC<IProfileTypes> = ({ children, name, image, text }) => {
    const [isLogoutClicked, setIsLogoutClicked] = useState(false)
    return (
        <SC.Wrapper>
            <Link to="/profile">
            <img src={image} />
            <SC.BlobWrapper>
                <SC.Blob />
                <SC.Blob />
                <SC.Blob />
            </SC.BlobWrapper>
                <SC.AccName>{name}</SC.AccName>
                <SC.AccTitle>{text}</SC.AccTitle>
            </Link>
            <ApolloConsumer>
                {client =>
                    <a onClick={() => {
                        if(isLogoutClicked) return;
                        setIsLogoutClicked(true)
                        client.mutate({
                            mutation: POST_LOGOUT
                        }).then(()=>{
                            setIsLogoutClicked(false)
                            navigate(0)
                        }).catch((e)=>{})
                    }}><small>Logout</small></a>
                }
            </ApolloConsumer>
        </SC.Wrapper>
    )
}

const LoginButton: FC = () => {
    return <SC.LoginButtonWrapper to='/login'><FontAwesomeIcon icon={faUserLock} style={{marginRight: '1rem'}}/>Login In</SC.LoginButtonWrapper>
}

export const LoginSideBar: FC = () => {
    const { loading, error, data } = useQuery(GET_PROFILE_SIDEBAR_CONTENT)
    if (loading) return <div></div>;
    if (error)
        if (error.message == 'Unauthorized')
            return <LoginButton />
        else
            return <div>`Error! {error.message}`</div>;
    const { photourl, designation, name } = data.me
    return (<Profile image={photourl} text={designation} name={name} />)
}
