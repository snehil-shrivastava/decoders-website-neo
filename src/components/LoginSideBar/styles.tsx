import styled, { keyframes } from "styled-components"

import { transparentize } from "polished"
import { Link } from "gatsby"

const fly = keyframes`
    40% {
      transform: translate(-6px, -6px);
    }
    60% {
      transform: translate(-12px, -2px);
    }
    100% {
      transform: translate(0px, 0px);
    }
`

export const BlobWrapper = styled.div`

`

export const Blob = styled.div`
    position: absolute;
    border-radius: 50%;
    -webkit-animation: ${fly} 5.8s linear infinite alternate;
    animation: ${fly} 5.8s linear infinite alternate;
    animation-delay: 0s;
    :nth-child(1) {
        width: 14px;
        height: 14px;
        top: 25px;
        left: 20px;
        background: #28327a;
        -webkit-animation-delay: 0.9s;
        animation-delay: 0.9s;
    }
    :nth-child(2) {
        width: 18px;
        height: 18px;
        background: #87344c;
        right: 20px;
        top: 20px;
        -webkit-animation-delay: 0.2s;
        animation-delay: 0.2s;
    }
    :nth-child(3) {
        width: 12px;
        height: 12px;
        background: #13645b;
        right: 35px;
        top: 55%;
        -webkit-animation-delay: 1.8s;
        animation-delay: 1.8s;
    }
`

export const AccName = styled.div`
margin: 20px 0 10px;
`

export const AccTitle = styled.div`
font-size: 14px;
`

export const Wrapper = styled.div`
margin: 0 20px 50px 0;
padding-bottom: 10px;
position: relative;
text-align: center;
position: relative;
img{
    width: 84px;
    height: 84px;
    border-radius: 50%;
    -o-object-fit: cover;
    object-fit: cover;
    -o-object-position: left;
    object-position: left;
    border: 3px solid #4255d3;
    padding: 5px;
}

border-bottom: ${props => transparentize(0.7, props.theme.sidebar.foreground)} solid medium;
`

export const SmallLink = styled.div`

`

export const LoginButtonWrapper = styled(Link)`
  display: inline-flex;
  align-items: center;
  font-family: "Montserrat", sans-serif;
  font-size: 24px;
  font-weight: 700;
  width: -moz-available;
  margin-right: 20px;
  text-decoration: none;
  text-transform: uppercase;
  background: #56718E;
  color: #fff;
  padding: 18px 28px;
  border-radius: 5px;
  transition: background 0.3s;
  cursor: pointer;
  box-shadow: 0 3px 18px rgba(0, 0, 0, 0.3);

  &:hover {
    background: #2C4E72;
  }
`

