import React, { FC } from "react"
import * as SC from "./styles"

export const HomePartner: FC = () => {
  return (
    <SC.HomePartnerWrapper>
      <a rel="noreferrer" target="_blank" href="https://sit.ac.in">
        <SC.StyledSitLogo alt="Sit" />
      </a>
      the official programming club of Siddaganga Institute of Technology, Tumkur, under the Department of Computer Science and Engineering.
    </SC.HomePartnerWrapper>
  )
}
