import _ from "lodash";
import React, { FC } from "react";
import { useDropzone } from "react-dropzone";

const UploadComponent : FC<{setFieldValue: (field: string, value: any) => void, activeTxt:string, inactiveTxt: string}> = (props) => {
    const { setFieldValue, activeTxt, inactiveTxt } = props;
    const { getRootProps, getInputProps, isDragActive } = useDropzone({
      multiple: false,
      accept: "image/*",
      maxSize: 1e6,
      onDrop: acceptedFiles => {
        setFieldValue("file", acceptedFiles[0]);
      },
      onDropRejected: (fileRejections) => {
        fileRejections.forEach(e => e.errors.forEach(x=> alert(x.message)))
      }
    });
    return (
      <div>
        {}
        <div {...getRootProps({ className: "dropzone" })}>
          <input {...getInputProps()} />
          {isDragActive ? (
            <p>{activeTxt}</p>
          ) : (
            <p>{inactiveTxt}</p>
          )}
        </div>
      </div>
    );
  };

export default UploadComponent;