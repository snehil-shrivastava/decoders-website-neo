import * as Yup from 'yup';

export const signinSchema = Yup.object().shape({
    username: Yup.string().min(3).max(20).required(),
    password: Yup.string().min(3).max(100).required(),
  });

export const signupSchema = Yup.object().shape({
    signinSchema,
    passwordConfirmation: Yup.string()
    .oneOf([Yup.ref('password'), null], 'Passwords must match'),
    name: Yup.string().min(3).max(20).required(),
    usn: Yup.string().matches(/1si[0-9]{2}[a-zA-Z]{2,3}[0-9]{3,3}/g).required(),
    file: Yup.mixed().required()
  });