import React from "react"
import useId from "../hooks/useId"
import sit from "./sit.png"

const SitLogo: React.FC = (props) => {
  const id = useId()

  return (
    <img src={sit} {...props}/>
  )
}

export default SitLogo
